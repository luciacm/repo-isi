import pytest
from sensor import Sensor
from check_sensor import check_sensor


def test_check_sensor(mocker):

	smock = mocker.patch('sensor.Sensor.read')

	lista_vacia = []
	mismo_numero = [2,2,2,2]
	un_numero = [5]
	muchos_numeros = [3,3,4,5,6,7,8,9,0,3,2,1,4,5,6,7,8]
	cortados = [3,3,3,5,5,5,3]
	
	check_sensor = mocker.Mock()
	
	def Atest():
		smock.return_value = lista_vacia
		check_sensor.return_value = 0
		assert check_sensor(Sensor()) == 0
	def Btest():
		smock.return_value = mismo_numero
		check_sensor.return_value = 4
		assert check_sensor(Sensor()) == 4
	def Ctest():
		smock.return_value = un_numero
		check_sensor.return_value = 1
		assert check_sensor(Sensor()) == 1
	def Dtest():
		smock.return_value = muchos_numeros
		check_sensor.return_value = 2
		assert check_sensor(Sensor()) == 2
	def Etest():
		smock.return_value = cortados
		check_sensor.return_value = 4
		assert check_sensor(Sensor()) == 4
	
