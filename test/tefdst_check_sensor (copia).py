import unittest
from unittest.mock import patch
from check_sensor import check_sensor
from sensor import Sensor

class test_check_sensor(unittest.TestCase):
	def test_A(self):
		with patch.object(Sensor, 'read', return_value=[]):
			s = Sensor()
			result = check_sensor(s)
			self.assertEqual(result,0)
		
		
if __name__ =='__main__':
	unittest.main()
	
